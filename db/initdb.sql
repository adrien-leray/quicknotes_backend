CREATE DATABASE quicknotes-db;

USE quicknotes-db;

CREATE TABLE note (
    id INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
    id_user INTEGER NOT NULL,
    title VARCHAR(50),
    body TINYTEXT,
    createdDate DATE,
    updatedDate DATE,
    FOREIGN KEY (id_user) REFERENCES user (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE tag (
    id INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
    title VARCHAR(50),
    body TINYTEXT,
    createdDate DATE,
    updatedDate DATE
);

CREATE TABLE tag_note (
    id_note INTEGER NOT NULL,
    id_tag INTEGER NOT NULL,
    FOREIGN KEY (id_note) REFERENCES note (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (id_tag) REFERENCES tag (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    PRIMARY KEY (id_note, id_tag)
);

CREATE TABLE user (
    id INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
    username VARCHAR(50),
    password VARCHAR(255),
    createdDate DATE,
    updatedDate DATE
);
