FROM python:3
ENV PYTHONUNBUFFERED=1
RUN mkdir /quicknotes_backend
WORKDIR /quicknotes_backend
COPY requirements.txt /quicknotes_backend/
RUN pip install -r requirements.txt
COPY . /quicknotes_backend/

