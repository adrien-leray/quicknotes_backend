# QUICKNOTES

[![pipeline status](https://gitlab.com/adrien-leray/quicknotes_backend/badges/master/pipeline.svg)](https://gitlab.com/adrien-leray/quicknotes_backend/-/commits/master) [![coverage report](https://gitlab.com/adrien-leray/quicknotes_backend/badges/master/coverage.svg)](https://gitlab.com/adrien-leray/quicknotes_backend/-/commits/master)

A simple quick note application to retrieve all your daily notes

## REQUIREMENTS

Check if you have docker installed, if not follow the [docker](https://www.docker.com/get-started) instructions to install it on your computer.

## GET STARTED

1. Just run `docker-compose up --build`
2. Have a look on <http://localhost:8000>
3. Enjoy the experience

## WORKING LOCALLY (ON YOUR REMOTE MACHINE)

1. Make sure you have all python tools installed
2. Install dependencies `pip install -r requirements.txt`
3. Run `python manage.py runserver`
2. Have a look on <http://localhost:8000>

## STOPING AND CLEANING CONTAINERS

Run `docker-compose down`

## RUNNER FOR THIS PROJECT

To create a runner for this project:

First install [gitlab-runner](https://docs.gitlab.com/runner/install/) following gitlab instructions

Then create a container for the runner with the following command:

```shell
docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image alpine:latest \
  --url "https://gitlab.com/" \
  --registration-token "your_token" \
  --description "a gitlab runner" \
  --tag-list "docker,linux" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
```