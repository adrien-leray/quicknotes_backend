from django.db import models
from django.utils import timezone

import datetime

class Tag(models.Model):
    title = models.CharField(max_length=50,null=False)
    body = models.TextField(null=True)
    createdDate = models.DateTimeField('Creation date',null=False)
    updatedDate = models.DateTimeField('Last update',null=False)

    def __str__(self):
        return self.title

class User(models.Model):
    username = models.CharField(max_length=50,null=False)
    password = models.CharField(max_length=255,null=False)
    createdDate = models.DateTimeField('Creation date',null=False)
    updatedDate = models.DateTimeField('Last update',null=False)

    def __str__(self):
        return self.username

class Note(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,null=False)
    title = models.CharField(max_length=50,null=True)
    body = models.TextField(null=False)
    createdDate = models.DateTimeField('Creation date',null=False)
    updatedDate = models.DateTimeField('Last update',null=False)
    tags = models.ManyToManyField(Tag,blank=True)

    def __str__(self):
        return self.body

    def was_published_recently(self):
        return self.createdDate >= timezone.now() - datetime.timedelta(days=1)