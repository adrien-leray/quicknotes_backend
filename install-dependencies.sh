## INTRO
# This script install a virtualenv and all dependencies to quickly start to work on the project for linux user

## REQUIREMENTS
# python3
# pip3
# a dependency config file => requirements.txt

## GET STARTED
# Run this script with "sh install-dependencies-linux.sh"

DEPENDENCIES_CONFIG=requirements.txt
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0'

if test -f "$DEPENDENCIES_CONFIG"; then
	pip3 install -r $DEPENDENCIES_CONFIG
	printf "${GREEN}Dependencies installation successfully completed ! ${NC}\n"
else
	printf "${RED}$DEPENDENCIES_CONFIG file is missing, make sure you have this file to install dependencies. ${NC}\n"
fi

